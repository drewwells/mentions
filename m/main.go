package main

import (
	"log"
	"net"
	"net/http"

	"bitbucket.org/57apps/mentions"
)

func main() {
	http.HandleFunc("/", mentions.MsgHandler)
	l, err := net.Listen("tcp4", ":3434")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("listening on:", l.Addr())
	log.Fatal(http.Serve(l, nil))
}

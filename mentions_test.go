package mentions

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestMentions(t *testing.T) {
	tms := []struct {
		in       string
		mentions []string
		eErr     error
	}{{
		in:       "@chris you around?",
		mentions: []string{"chris"},
	}}

	for _, tm := range tms {
		resp, err := ParseMsg(tm.in)
		if err != tm.eErr {
			t.Fatalf("got: %s wanted: %s", err, tm.eErr)
		}

		if err != nil {
			continue
		}

		if !reflect.DeepEqual(tm.mentions, resp.Mentions) {
			t.Errorf("got: %#v\nwanted: % #v\n", resp.Mentions, tm.mentions)
		}

	}

}

func TestEmoticons(t *testing.T) {
	tms := []struct {
		in        string
		emoticons []string
		eErr      error
	}{{
		in:        "Good morning! (megusta) (coffee)",
		emoticons: []string{"megusta", "coffee"},
	}}

	for _, tm := range tms {
		resp, err := ParseMsg(tm.in)
		if err != tm.eErr {
			t.Fatalf("got: %s wanted: %s", err, tm.eErr)
		}

		if err != nil {
			continue
		}

		if !reflect.DeepEqual(tm.emoticons, resp.Emoticons) {
			t.Errorf("got: %#v\nwanted: % #v\n", resp.Emoticons, tm.emoticons)
		}

	}

}

func fetchHandler(body string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(body))
	})
}

func TestFetch(t *testing.T) {
	tms := []struct {
		title string
		body  string
		eErr  error
	}{{
		title: "2016 Rio Olympic Games | NBC Olympics",
		body:  "<html><title>2016 Rio Olympic Games | NBC Olympics</title></html>",
	}, {
		body: "<html><title></title></html>",
		eErr: ErrTitleInvalid,
	},
	}

	for _, tm := range tms {

		ts := httptest.NewServer(fetchHandler(tm.body))
		defer ts.Close()

		link, err := FetchPage(ts.URL)
		if err != tm.eErr {
			t.Fatalf("got: %s wanted: %s", err, tm.eErr)
		}

		if e := tm.title; link.Title != e {
			t.Errorf("got: %s wanted: %s", link.Title, e)
		}

		eLink := Link{URL: ts.URL, Title: tm.title}
		if !reflect.DeepEqual(eLink, link) {
			t.Errorf("got: %#v\nwanted: %#v", link, eLink)
		}
	}
}

func TestReadTitle(t *testing.T) {
	tms := []struct {
		body  string
		title string
		eErr  error
	}{{
		body:  "<html><title>The title</title></html>",
		title: "The title",
	}, {
		body:  "<html><title attr='value'>The title</title></html>",
		title: "The title",
	}, {
		body: "<html><title/>The title</html>",
		eErr: ErrTitleNotFound,
	}, {
		body: "<html><title></title></html>",
		eErr: ErrTitleInvalid,
	}}

	for _, tm := range tms {
		title, err := ReadTitle(bytes.NewBufferString(tm.body))
		if err != tm.eErr {
			t.Fatalf("got: %s wanted: %s", err, tm.eErr)
		}
		if err != nil {
			continue
		}

		if e := tm.title; e != title {
			t.Errorf("got: %q wanted: %q", title, e)
		}
	}
}

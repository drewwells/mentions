// Package mentions handles web requests parsing the input string for
// mentions @username, emoticons (fireworks), and links http://example.com
//
// It can be used to build some metadata about a phrase sent to it.
// For example,
//
//
//   curl -q -X POST localhost:3434 --data '\@bob \@john (success) such \
//     a cool feature; https://twitter.com/jdorfman/status/430511497475670016' \
//     | jq '.'
//
//  {
//    "mentions": [
//      "bob",
//      "john"
//    ],
//    "emoticons": [
//      "success"
//    ],
//    "links": [
//      {
//        "url": "https://twitter.com/jdorfman/status/430511497475670016",
//        "title": "Justin Dorfman on Twitter: &amp;#34;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&amp;#34;"
//      }
//    ]
//  }
//
package mentions

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"

	"golang.org/x/net/html"
)

// Link represents a mentioned link of a message. The title is parsed
// from the live URL
type Link struct {
	URL   string `json:"url"`
	Title string `json:"title"`
}

// Response is the return of MsgHandler
type Response struct {
	Mentions  []string `json:"mentions"`
	Emoticons []string `json:"emoticons"`
	Links     []Link   `json:"links"`
}

// MsgHandler reads a string from the body of text, returning
// a JSON encoded Response.
func MsgHandler(w http.ResponseWriter, r *http.Request) {

	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := ParseMsg(string(bs))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	enc := json.NewEncoder(w)
	if err := enc.Encode(resp); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// REUsername matches a mentioned username. From testing, the username
// has a long max length so don't enforce any limits here.
var REUsername = regexp.MustCompile("@\\w+")

// REEmot matches emoticon format with the following requirements
// (, 15 alphanumeric characeters, )
var REEmot = regexp.MustCompile("\\(\\w{1,15}\\)")

// RELink is a weak regexp for URLs
var RELink = regexp.MustCompile("https?:\\/\\/\\S+")

// ParseMsg accepts the input string and runs through a series of regex
// to locate links, emoticons and mentions in the text.
func ParseMsg(in string) (resp Response, err error) {

	if names := REUsername.FindAllString(in, -1); names != nil {
		for _, name := range names {
			resp.Mentions = append(resp.Mentions, strings.TrimPrefix(name, "@"))
		}
	}

	if emotes := REEmot.FindAllString(in, -1); emotes != nil {
		for _, emote := range emotes {
			em := strings.TrimSuffix(strings.TrimPrefix(emote, "("), ")")
			resp.Emoticons = append(resp.Emoticons, em)
		}
	}

	if links := RELink.FindAllString(in, -1); links != nil {
		for _, l := range links {
			link, err := FetchPage(l)
			if err != nil {
				log.Println(err)
				continue
			}
			resp.Links = append(resp.Links, link)
		}
	}

	return
}

// FetchPage fetchs the mentioned URL and attempts to read the title from
// it
func FetchPage(path string) (Link, error) {
	resp, err := http.Get(path)
	if err != nil {
		return Link{}, err
	}
	defer resp.Body.Close()
	title, err := ReadTitle(resp.Body)
	return Link{path, title}, err
}

// ErrTitleNotFound indicates that the entire HTML document was scanned
// and a title was not found.
var ErrTitleNotFound = errors.New("title not found")

// ErrTitleInvalid indicates that the title is blank.
var ErrTitleInvalid = errors.New("title is invalid")

// ReadTitle accepts a Reader of an HTML document. The HTML
// tokenizer then walks through the HTML tree attempting to locate
// text inside a title tag.
func ReadTitle(body io.Reader) (string, error) {
	z := html.NewTokenizer(body)
	for {
		tt := z.Next()
		switch {
		case tt == html.ErrorToken:
			// end of document
			return "", ErrTitleNotFound
		case tt == html.StartTagToken:
			t := z.Token()
			isLink := t.Data == "title"
			if isLink {
				// Attempt tp find the text within Title
				t := z.Next()
				if t == html.TextToken {
					return html.EscapeString(z.Token().String()), nil
				}
				return "", ErrTitleInvalid
			}
		}
	}
}
